<?php

namespace EmailNotificationsComponent\Plugins\SendMainOnPublishNews\App;

use modMail;
use modX;

class Main
{
    public function run(modX $modx, $resource)
    {
        $modx->getService('mail', 'mail.modPHPMailer');
        $modx->mail->set(modMail::MAIL_BODY, 'News article description: ' . $resource->description);
        $modx->mail->set(modMail::MAIL_FROM, 'me@example.org');
        $modx->mail->set(modMail::MAIL_FROM_NAME, 'Johnny Tester');
        $modx->mail->set(modMail::MAIL_SUBJECT, 'New article has been published: ' . $resource->pagetitle);
        $modx->mail->address('to', 'user@example.com');
        $modx->mail->address('reply-to', 'me@xexample.org');

        if (!$modx->mail->send()) {
            $modx->log(modX::LOG_LEVEL_ERROR, 'An error occurred while trying to send the email: ' . $modx->mail->mailer->ErrorInfo);
        }
        $modx->mail->reset();
    }
}
