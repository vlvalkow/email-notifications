<?php
/**
 * Autoloader
 */
require MODX_BASE_PATH . 'core/components/emailnotifications/elements/plugins/newspublishedemail/autoloader.php';

$autoloader = new EmailNotificationsComponent\Plugins\SendMainOnPublishNews\Psr4Autoloader();

$autoloader->register();

$autoloader->addNamespace(
    'EmailNotificationsComponent\Plugins\SendMainOnPublishNews\App',
    MODX_BASE_PATH . 'core/components/emailnotifications/elements/plugins/newspublishedemail/app'
);

$autoloader->addNamespace(
    'EmailNotificationsComponent\Plugins\SendMainOnPublishNews\Tests',
    MODX_BASE_PATH . 'core/components/emailnotifications/elements/plugins/newspublishedemail/tests'
);

/**
 * Initialise plugin
 */
$plugin = new EmailNotificationsComponent\Plugins\SendMainOnPublishNews\App\Main();

$plugin->run($modx, $resource);
