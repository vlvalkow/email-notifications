# Email Notifications
A component for ModX that sends an email when a resource is saved.

## Prerequisites
1. ModX installation.
2. [Composer](https://getcomposer.org/).
3. [MailHog](https://github.com/mailhog/MailHog).

## Setup
1. cd /path/to/modx/core/components/ 
2. git clone https://vlvalkow@bitbucket.org/vlvalkow/email-notifications.git
3. cd emailnotifications
4. composer intall
5. phpunit
