<?php

namespace EmailNotificationsComponent\Plugins\SendMainOnPublishNews\Tests;

/**
 * Reference: https://github.com/BobRay/MyComponent/tree/master/_build
 */
abstract class MODxTestCase extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \modX $modx
     */
    public $modx = null;

    public function setUp()
    {
        require_once MODX_BASE_PATH . 'core/components/emailnotifications/tests/tests.config.php';
        require_once MODX_BASE_PATH . 'core/model/modx/modx.class.php';

        $this->modx = new \modX();
        $this->modx->initialize('web');
        $this->modx->getService('error', 'error.modError', '', '');
    }

    public function tearDown()
    {
        $this->modx = null;
    }
}
