<?php

$domain = 'modx.local';

if (!defined('MODX_CORE_PATH')) {
    define('MODX_BASE_PATH', '/home/vagrant/code/modx/');
    define('MODX_CORE_PATH', MODX_BASE_PATH . '/core/');
    define('MODX_MANAGER_PATH', MODX_BASE_PATH . 'manager/');
    define('MODX_CONNECTORS_PATH', MODX_BASE_PATH . 'connectors/');
    define('MODX_ASSETS_PATH', MODX_BASE_PATH . 'assets/');
}

/* not used -- here to prevent E_NOTICE warnings */
if (!defined('MODX_BASE_URL')) {
    define('MODX_BASE_URL', 'http://' . $domain . '/addons/');
    define('MODX_MANAGER_URL', 'http://' . $domain . '/addons/manager/');
    define('MODX_ASSETS_URL', 'http://' . $domain . '/addons/assets/');
    define('MODX_CONNECTORS_URL', 'http://' . $domain . '/addons/connectors/');
}
