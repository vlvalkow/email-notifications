<?php

namespace EmailNotificationsComponent\Plugins\SendMainOnPublishNews\Tests;

use GuzzleHttp\Client;

class NewsPublishedEmailPluginTest extends MODxTestCase
{
    public $mailHog = null;

    public function setUp()
    {
        parent::setUp();

        $this->mailHog = new Client(array(
            'base_uri' => 'http://127.0.0.1:8025/api/v2/'
        ));
    }

    public function tearDown()
    {
        parent::tearDown();

        $this->mailHog = null;
    }

    public function getMessages()
    {
        $jsonResponse = $this->mailHog->get('messages');
        return json_decode($jsonResponse->getBody());
    }

    public function getLastMessage()
    {
        $messages = $this->getMessages();
        if (empty($messages)) {
            $this->fail("No messages received");
        }

        // messages are in descending order
        return reset($messages->items);
    }

    public function test_it_sends_an_email_on_form_save()
    {
        $resource = $this->modx->newObject('modResource');
        $resource->fromArray(array(
            'pagetitle' => 'My awesome news article',
            'description' => 'A test resource.',
        ));

        // TODO: Figure out how to use a test database.
        // Optionally save to the database. We don't necessarily need it for this test.
        //if ($resource->save() === false) {
        //    die('An error occurred while saving!');
        //}

        // Fire the event that the plugin is hooked into.
        $this->modx->invokeEvent('OnDocFormSave', array(
            'mode' => \modSystemEvent::MODE_NEW,
            'id' => $resource->get('id'),
            'resource' => $resource,
            'reloadOnly' => false,
        ));

        $email = $this->getLastMessage();

        // from
        $this->assertEquals('me@example.org', $email->Raw->From);
        // to
        $this->assertEquals('user@example.com', $email->Raw->To[0]);
        // email subject contains resource title
        $this->assertTrue(boolval(strpos($email->Content->Headers->Subject[0], $resource->pagetitle)));
        // email body contains resource description
        $this->assertTrue(boolval(strpos($email->Raw->Data, $resource->description)));
    }
}
